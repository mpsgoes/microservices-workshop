package com.sensedia.customer.adapters.dtos;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Validated
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
@NoArgsConstructor
public class CustomerCreationDto {

  @NotNull private String firstName;

  @NotNull private String lastName;

  @NotNull private String document;

  @NotNull private BigDecimal grossSalary;

}
